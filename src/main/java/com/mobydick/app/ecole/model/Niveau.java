package com.mobydick.app.ecole.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.OneToMany;

public class Niveau {
	
	public String idNiveau;
	
	public String nomNiveau;
	
	@OneToMany(cascade = CascadeType.ALL)
	public List<Cours> listeCours;

}
